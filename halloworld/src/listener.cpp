#include <iostream>
#include <ros/ros.h>
#include <std_msgs/String.h>

using namespace std;
// 定义回调函数
void listener_callback(const std_msgs::String::ConstPtr& msg)
{
    ROS_INFO("I heard : [%s]" , msg->data.c_str()); // String 转为 str字符串
}

int main( int argc, char** argv)
{
    // 创建节点
    ros::init(argc, argv, "listener_node");
    ros::NodeHandle n;

    // 订阅话题 以及指定该话题的回调函数
    ros::Subscriber listenerSub = n.subscribe("chatter", 10,listener_callback);

    ros::spin();

    return 0;
}

