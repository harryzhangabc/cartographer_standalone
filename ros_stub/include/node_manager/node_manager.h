#ifndef NODE_MANAGER_H
#define NODE_MANAGER_H

#include <vector>
#include <ros/ros.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_ros/transform_listener.h>
#include <urdf/model.h>

class Node_manager{
    public:
    Node_manager();
    ~Node_manager();

    typedef struct{
        std::string _topic;
        void (*fp);
    }WoZiJiDingDeLaiDaWoA;


    void add_node(WoZiJiDingDeLaiDaWoA& ops);
    bool GetMemoryAddr(void);
    void DestroyMemory(void);
    void ReadMemory(void);
    void CreateMemory(void);
    void WriteMemory(std::string s);

    private:

    std::vector<WoZiJiDingDeLaiDaWoA> node_buffter;

};

#endif