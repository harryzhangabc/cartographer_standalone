#include <ros/ros.h>
#include <signal.h>

using namespace std;
Node_manager node;

void Handle(int s){
  if(s == 2)
  {
      node.DestroyMemory();
      exit(0);
  }
} 

int main()
{
    string a;
    a.resize(100); 
    signal(2,Handle);
    node.CreateMemory();
    while(1)
    {
        cout << "Ultimate Super Memory Test: " << endl;
        scanf("%s", &a[0]);
        node.WriteMemory(a);
    }
    return 0;
}