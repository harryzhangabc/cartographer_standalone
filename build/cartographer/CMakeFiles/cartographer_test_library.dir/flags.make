# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.10

# compile CXX with /usr/bin/c++
CXX_FLAGS = -O3 -DNDEBUG    -pthread -std=c++11 -fPIC  -Wall -Wpedantic -Werror=format-security -Werror=missing-braces -Werror=reorder -Werror=return-type -Werror=switch -Werror=uninitialized -O3 -DNDEBUG -std=gnu++14

CXX_DEFINES = -DGFLAGS_IS_A_DLL=0

CXX_INCLUDES = -I/mnt/d/cartographer_standalone/rosbridgecpp -isystem /usr/src/googletest/googlemock/gtest/include -isystem /usr/include/eigen3 -isystem /usr/include/lua5.2 -isystem /usr/include/cairo -isystem /usr/include/glib-2.0 -isystem /usr/lib/x86_64-linux-gnu/glib-2.0/include -isystem /usr/include/pixman-1 -isystem /usr/include/freetype2 -isystem /usr/include/libpng16 -I/mnt/d/cartographer_standalone/build/cartographer -I/mnt/d/cartographer_standalone/cartographer 

