#include <iostream>
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sstream>

using namespace std;

int main( int argc, char** argv )
{
    // 创建节点
    ros::init(argc, argv, "talker_node");
    ros::NodeHandle n;

    // 发布话题
    ros::Publisher talkerPub = n.advertise<std_msgs::String>("chatter", 10);

    // 设置发送频率
    // ros::Rate loop_rate(10);

    int count = 0;
    while(ros::ok())
    {

        std_msgs::String msg;

        std::stringstream ss;
        ss << "hello world" <<count; // 将“hello world” 和 count 一起输出到 string流中

        msg.data = ss.str(); // String 转为 str 作为消息体的数据

        ROS_INFO("%s Modified", msg.data.c_str());

        talkerPub.publish(msg);

        // ros::spinOnce();

        // loop_rate.sleep();

        count ++;
    }

    return 0;
}
